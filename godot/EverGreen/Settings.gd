#class_name Settings
extends Resource

const DATA_VERSION = 0
const __MAX_SIGNALS = 255  # + 1 is the real max because S0 is a possible signal.

signal saving_finished
signal saving

var __data = {}  # {"dummy": [1234, "S00"]}
var auto_save = false
var __additional_save = false
var __save_thread = null
var save_path = ""
var network_sync = false
var name = ""
var load_on_ready = false
var __saving = false
var _being_tested = false
var __requesting_quit = false
var no_save_thread = false

func _init(the_set_name:String,path:String="",load_on_init:bool=false, 
set_auto_save:bool=false,set_network_sync:bool=false):
    # Using _init_ so that the code can be tested by GdUnit3.
    ensure_statics()
    name = the_set_name
    if path == "":
        save_path = the_set_name
    else:
        save_path = path
    auto_save = set_auto_save
    network_sync = set_network_sync
    load_on_ready = load_on_init
    if load_on_ready:
        load_data_on_ready()
        
func ensure_statics():
    if PM.statics.has("Settings_signals_used"):
        return
    else:   
        PM.statics["Settings_signals_used"] = 0

func reset_for_testing() -> void:
    PM.statics.Settings_signals_used = -1

func load_data_on_ready():
    if load_on_ready:
        var result = load_data()
        if result == FAILED:
            reg_("DATA_VERSION", DATA_VERSION, false)
    else:
        reg_("DATA_VERSION", DATA_VERSION, false)
    
func set_(data_name, new_val, emit_a_signal=true, _called_by_sync=false) -> bool:
    reg_(data_name, new_val)
    if emit_a_signal:
        emit_signal(__data[data_name][1], __data[data_name][0])
    if auto_save:
        save_settings()
    return true
    
func get_(data_name):
    if __data.has(data_name):
        return __data[data_name][0]
    else:
        return null
    
func reg_(data_name, new_val, add_signal=true) -> bool:
    # Note: register_() does not emit a signal even if the data value is changed.
    if __data.has(data_name):
        __data[data_name][0] = new_val
    else:
        if add_signal:
            __data[data_name] = [new_val, __create_signal()]
        else:
            __data[data_name] = [new_val, "Sig00"]
    return true

func register(data_name, new_val, add_signal=true) -> bool:
    return reg_(data_name, new_val, add_signal)
       
func mon_(data_name) -> String:
    if not __data.has(data_name):
        reg_(data_name, null)
    return __data[data_name][1]  # return signal to monitor.

func monitor(data_name) -> String:
    return mon_(data_name)

func save_settings() -> void:
    __saving = true
    emit_signal("saving")
    if __save_thread == null:
        __save_thread = Thread.new()
        call_deferred("__deferred_save")
    else:
        __additional_save = true
            
func __deferred_save() -> void:
    if no_save_thread:
        __threaded_save()
    else:
        __save_thread.start(Callable(self,"__threaded_save"))
    
func __threaded_save() -> void:
    var file = FileAccess.open("user://" + save_path + ".json", FileAccess.WRITE)
    file.store_string(JSON.stringify(__data))
    call_deferred("__call_cleanup")
    
func __call_cleanup() -> void:
    if not no_save_thread:
        __save_thread.wait_to_finish()
    __save_thread = null
    if __additional_save:
        __additional_save = false
        save_settings()
    else:
        __saving = false
        emit_signal("saving_finished")

func load_data():
    if FileAccess.file_exists("user://" + save_path + ".json"):
        var file = FileAccess.open("user://" + save_path + ".json", FileAccess.READ)
        var text = file.get_as_text()
        var test_json_conv = JSON.new()
        test_json_conv.parse(text)
        var temp = test_json_conv.get_data()
        if temp != null:
            if temp.has("DATA_VERSION"):
                if temp["DATA_VERSION"][0] == DATA_VERSION:
                    # We need to remap all of the hard code saved signals 
                    # to the currently available signals.
                    for data_name in temp:
                        if data_name != "DATA_VERSION":
                            __data[data_name] = [temp[data_name][0], __create_signal()]
                        else:
                            __data[data_name] = [temp[data_name][0], "Sig00"]
                    return OK
                else:
                    # In the future we can enable data upgrades.
                    return FAILED
            else:
                return FAILED
        else:
            return FAILED
    else:
        return FAILED
            
func __create_signal() -> String:
    PM.statics.Settings_signals_used += 1
    if PM.statics.Settings_signals_used > __MAX_SIGNALS:
        __requesting_quit = true
        if not _being_tested:
            PM.get_tree().quit()
    return "Sig" + str(PM.statics.Settings_signals_used)

######## BEGIN SIGNALS ########
# warning-ignore:unused_signal
signal Sig00
# warning-ignore:unused_signal
signal Sig0
# warning-ignore:unused_signal
signal Sig1
# warning-ignore:unused_signal
signal Sig2
# warning-ignore:unused_signal
signal Sig3
# warning-ignore:unused_signal
signal Sig4
# warning-ignore:unused_signal
signal Sig5
# warning-ignore:unused_signal
signal Sig6
# warning-ignore:unused_signal
signal Sig7
# warning-ignore:unused_signal
signal Sig8
# warning-ignore:unused_signal
signal Sig9
# warning-ignore:unused_signal
signal Sig10
# warning-ignore:unused_signal
signal Sig11
# warning-ignore:unused_signal
signal Sig12
# warning-ignore:unused_signal
signal Sig13
# warning-ignore:unused_signal
signal Sig14
# warning-ignore:unused_signal
signal Sig15
# warning-ignore:unused_signal
signal Sig16
# warning-ignore:unused_signal
signal Sig17
# warning-ignore:unused_signal
signal Sig18
# warning-ignore:unused_signal
signal Sig19
# warning-ignore:unused_signal
signal Sig20
# warning-ignore:unused_signal
signal Sig21
# warning-ignore:unused_signal
signal Sig22
# warning-ignore:unused_signal
signal Sig23
# warning-ignore:unused_signal
signal Sig24
# warning-ignore:unused_signal
signal Sig25
# warning-ignore:unused_signal
signal Sig26
# warning-ignore:unused_signal
signal Sig27
# warning-ignore:unused_signal
signal Sig28
# warning-ignore:unused_signal
signal Sig29
# warning-ignore:unused_signal
signal Sig30
# warning-ignore:unused_signal
signal Sig31
# warning-ignore:unused_signal
signal Sig32
# warning-ignore:unused_signal
signal Sig33
# warning-ignore:unused_signal
signal Sig34
# warning-ignore:unused_signal
signal Sig35
# warning-ignore:unused_signal
signal Sig36
# warning-ignore:unused_signal
signal Sig37
# warning-ignore:unused_signal
signal Sig38
# warning-ignore:unused_signal
signal Sig39
# warning-ignore:unused_signal
signal Sig40
# warning-ignore:unused_signal
signal Sig41
# warning-ignore:unused_signal
signal Sig42
# warning-ignore:unused_signal
signal Sig43
# warning-ignore:unused_signal
signal Sig44
# warning-ignore:unused_signal
signal Sig45
# warning-ignore:unused_signal
signal Sig46
# warning-ignore:unused_signal
signal Sig47
# warning-ignore:unused_signal
signal Sig48
# warning-ignore:unused_signal
signal Sig49
# warning-ignore:unused_signal
signal Sig50
# warning-ignore:unused_signal
signal Sig51
# warning-ignore:unused_signal
signal Sig52
# warning-ignore:unused_signal
signal Sig53
# warning-ignore:unused_signal
signal Sig54
# warning-ignore:unused_signal
signal Sig55
# warning-ignore:unused_signal
signal Sig56
# warning-ignore:unused_signal
signal Sig57
# warning-ignore:unused_signal
signal Sig58
# warning-ignore:unused_signal
signal Sig59
# warning-ignore:unused_signal
signal Sig60
# warning-ignore:unused_signal
signal Sig61
# warning-ignore:unused_signal
signal Sig62
# warning-ignore:unused_signal
signal Sig63
# warning-ignore:unused_signal
signal Sig64
# warning-ignore:unused_signal
signal Sig65
# warning-ignore:unused_signal
signal Sig66
# warning-ignore:unused_signal
signal Sig67
# warning-ignore:unused_signal
signal Sig68
# warning-ignore:unused_signal
signal Sig69
# warning-ignore:unused_signal
signal Sig70
# warning-ignore:unused_signal
signal Sig71
# warning-ignore:unused_signal
signal Sig72
# warning-ignore:unused_signal
signal Sig73
# warning-ignore:unused_signal
signal Sig74
# warning-ignore:unused_signal
signal Sig75
# warning-ignore:unused_signal
signal Sig76
# warning-ignore:unused_signal
signal Sig77
# warning-ignore:unused_signal
signal Sig78
# warning-ignore:unused_signal
signal Sig79
# warning-ignore:unused_signal
signal Sig80
# warning-ignore:unused_signal
signal Sig81
# warning-ignore:unused_signal
signal Sig82
# warning-ignore:unused_signal
signal Sig83
# warning-ignore:unused_signal
signal Sig84
# warning-ignore:unused_signal
signal Sig85
# warning-ignore:unused_signal
signal Sig86
# warning-ignore:unused_signal
signal Sig87
# warning-ignore:unused_signal
signal Sig88
# warning-ignore:unused_signal
signal Sig89
# warning-ignore:unused_signal
signal Sig90
# warning-ignore:unused_signal
signal Sig91
# warning-ignore:unused_signal
signal Sig92
# warning-ignore:unused_signal
signal Sig93
# warning-ignore:unused_signal
signal Sig94
# warning-ignore:unused_signal
signal Sig95
# warning-ignore:unused_signal
signal Sig96
# warning-ignore:unused_signal
signal Sig97
# warning-ignore:unused_signal
signal Sig98
# warning-ignore:unused_signal
signal Sig99
# warning-ignore:unused_signal
signal Sig100
# warning-ignore:unused_signal
signal Sig101
# warning-ignore:unused_signal
signal Sig102
# warning-ignore:unused_signal
signal Sig103
# warning-ignore:unused_signal
signal Sig104
# warning-ignore:unused_signal
signal Sig105
# warning-ignore:unused_signal
signal Sig106
# warning-ignore:unused_signal
signal Sig107
# warning-ignore:unused_signal
signal Sig108
# warning-ignore:unused_signal
signal Sig109
# warning-ignore:unused_signal
signal Sig110
# warning-ignore:unused_signal
signal Sig111
# warning-ignore:unused_signal
signal Sig112
# warning-ignore:unused_signal
signal Sig113
# warning-ignore:unused_signal
signal Sig114
# warning-ignore:unused_signal
signal Sig115
# warning-ignore:unused_signal
signal Sig116
# warning-ignore:unused_signal
signal Sig117
# warning-ignore:unused_signal
signal Sig118
# warning-ignore:unused_signal
signal Sig119
# warning-ignore:unused_signal
signal Sig120
# warning-ignore:unused_signal
signal Sig121
# warning-ignore:unused_signal
signal Sig122
# warning-ignore:unused_signal
signal Sig123
# warning-ignore:unused_signal
signal Sig124
# warning-ignore:unused_signal
signal Sig125
# warning-ignore:unused_signal
signal Sig126
# warning-ignore:unused_signal
signal Sig127
# warning-ignore:unused_signal
signal Sig128
# warning-ignore:unused_signal
signal Sig129
# warning-ignore:unused_signal
signal Sig130
# warning-ignore:unused_signal
signal Sig131
# warning-ignore:unused_signal
signal Sig132
# warning-ignore:unused_signal
signal Sig133
# warning-ignore:unused_signal
signal Sig134
# warning-ignore:unused_signal
signal Sig135
# warning-ignore:unused_signal
signal Sig136
# warning-ignore:unused_signal
signal Sig137
# warning-ignore:unused_signal
signal Sig138
# warning-ignore:unused_signal
signal Sig139
# warning-ignore:unused_signal
signal Sig140
# warning-ignore:unused_signal
signal Sig141
# warning-ignore:unused_signal
signal Sig142
# warning-ignore:unused_signal
signal Sig143
# warning-ignore:unused_signal
signal Sig144
# warning-ignore:unused_signal
signal Sig145
# warning-ignore:unused_signal
signal Sig146
# warning-ignore:unused_signal
signal Sig147
# warning-ignore:unused_signal
signal Sig148
# warning-ignore:unused_signal
signal Sig149
# warning-ignore:unused_signal
signal Sig150
# warning-ignore:unused_signal
signal Sig151
# warning-ignore:unused_signal
signal Sig152
# warning-ignore:unused_signal
signal Sig153
# warning-ignore:unused_signal
signal Sig154
# warning-ignore:unused_signal
signal Sig155
# warning-ignore:unused_signal
signal Sig156
# warning-ignore:unused_signal
signal Sig157
# warning-ignore:unused_signal
signal Sig158
# warning-ignore:unused_signal
signal Sig159
# warning-ignore:unused_signal
signal Sig160
# warning-ignore:unused_signal
signal Sig161
# warning-ignore:unused_signal
signal Sig162
# warning-ignore:unused_signal
signal Sig163
# warning-ignore:unused_signal
signal Sig164
# warning-ignore:unused_signal
signal Sig165
# warning-ignore:unused_signal
signal Sig166
# warning-ignore:unused_signal
signal Sig167
# warning-ignore:unused_signal
signal Sig168
# warning-ignore:unused_signal
signal Sig169
# warning-ignore:unused_signal
signal Sig170
# warning-ignore:unused_signal
signal Sig171
# warning-ignore:unused_signal
signal Sig172
# warning-ignore:unused_signal
signal Sig173
# warning-ignore:unused_signal
signal Sig174
# warning-ignore:unused_signal
signal Sig175
# warning-ignore:unused_signal
signal Sig176
# warning-ignore:unused_signal
signal Sig177
# warning-ignore:unused_signal
signal Sig178
# warning-ignore:unused_signal
signal Sig179
# warning-ignore:unused_signal
signal Sig180
# warning-ignore:unused_signal
signal Sig181
# warning-ignore:unused_signal
signal Sig182
# warning-ignore:unused_signal
signal Sig183
# warning-ignore:unused_signal
signal Sig184
# warning-ignore:unused_signal
signal Sig185
# warning-ignore:unused_signal
signal Sig186
# warning-ignore:unused_signal
signal Sig187
# warning-ignore:unused_signal
signal Sig188
# warning-ignore:unused_signal
signal Sig189
# warning-ignore:unused_signal
signal Sig190
# warning-ignore:unused_signal
signal Sig191
# warning-ignore:unused_signal
signal Sig192
# warning-ignore:unused_signal
signal Sig193
# warning-ignore:unused_signal
signal Sig194
# warning-ignore:unused_signal
signal Sig195
# warning-ignore:unused_signal
signal Sig196
# warning-ignore:unused_signal
signal Sig197
# warning-ignore:unused_signal
signal Sig198
# warning-ignore:unused_signal
signal Sig199
# warning-ignore:unused_signal
signal Sig200
# warning-ignore:unused_signal
signal Sig201
# warning-ignore:unused_signal
signal Sig202
# warning-ignore:unused_signal
signal Sig203
# warning-ignore:unused_signal
signal Sig204
# warning-ignore:unused_signal
signal Sig205
# warning-ignore:unused_signal
signal Sig206
# warning-ignore:unused_signal
signal Sig207
# warning-ignore:unused_signal
signal Sig208
# warning-ignore:unused_signal
signal Sig209
# warning-ignore:unused_signal
signal Sig210
# warning-ignore:unused_signal
signal Sig211
# warning-ignore:unused_signal
signal Sig212
# warning-ignore:unused_signal
signal Sig213
# warning-ignore:unused_signal
signal Sig214
# warning-ignore:unused_signal
signal Sig215
# warning-ignore:unused_signal
signal Sig216
# warning-ignore:unused_signal
signal Sig217
# warning-ignore:unused_signal
signal Sig218
# warning-ignore:unused_signal
signal Sig219
# warning-ignore:unused_signal
signal Sig220
# warning-ignore:unused_signal
signal Sig221
# warning-ignore:unused_signal
signal Sig222
# warning-ignore:unused_signal
signal Sig223
# warning-ignore:unused_signal
signal Sig224
# warning-ignore:unused_signal
signal Sig225
# warning-ignore:unused_signal
signal Sig226
# warning-ignore:unused_signal
signal Sig227
# warning-ignore:unused_signal
signal Sig228
# warning-ignore:unused_signal
signal Sig229
# warning-ignore:unused_signal
signal Sig230
# warning-ignore:unused_signal
signal Sig231
# warning-ignore:unused_signal
signal Sig232
# warning-ignore:unused_signal
signal Sig233
# warning-ignore:unused_signal
signal Sig234
# warning-ignore:unused_signal
signal Sig235
# warning-ignore:unused_signal
signal Sig236
# warning-ignore:unused_signal
signal Sig237
# warning-ignore:unused_signal
signal Sig238
# warning-ignore:unused_signal
signal Sig239
# warning-ignore:unused_signal
signal Sig240
# warning-ignore:unused_signal
signal Sig241
# warning-ignore:unused_signal
signal Sig242
# warning-ignore:unused_signal
signal Sig243
# warning-ignore:unused_signal
signal Sig244
# warning-ignore:unused_signal
signal Sig245
# warning-ignore:unused_signal
signal Sig246
# warning-ignore:unused_signal
signal Sig247
# warning-ignore:unused_signal
signal Sig248
# warning-ignore:unused_signal
signal Sig249
# warning-ignore:unused_signal
signal Sig250
# warning-ignore:unused_signal
signal Sig251
# warning-ignore:unused_signal
signal Sig252
# warning-ignore:unused_signal
signal Sig253
# warning-ignore:unused_signal
signal Sig254
# warning-ignore:unused_signal
signal Sig255
# warning-ignore:unused_signal
signal Sig256
######## END SIGNALS ########
