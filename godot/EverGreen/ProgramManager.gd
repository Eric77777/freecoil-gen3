extends Node2D

const GODOT_MAX_INT: int = 9223372036854775807

# Major.Minor.Micro-alpha, beta, rc, preview, dev
var VERSION = "0.5.0-dev0"
var VERSION_INT = 5  # Increment any time the micro changes.
var version_mismatch = false
var update_boot_image: bool = false
const DEBUG_LEVELS = ["not_set", "debug", "info", "warning", "error", 
    "critical", "testing"
]

var playing_custom_scene = false
var running_as_export = false
var debug_level = 3
var debug_output_to_gui = false
var ci_testing = false

var operating_system: String = ""
var max_threads: int = 1  # Always tries to subtract one for the main thread.
var current_threads: int = 0  # Does not count the main thread.
var display_metrics: Dictionary = {}
var unique_id: String = ""

var erase_user_dir:bool = false
var ran_erase_user_data_dir: bool = false
var artificial_delay_between_states: float = 0
var fake_os: String = ""
var fake_unique_id: String = ""
var run_unit_tests: bool = false
var run_specified_test: bool = false

var States: Dictionary = {}
var BGLoad: Object = preload("res://EverGreen/BackGroundLoading.gd").new()
var PD: Resource
var PersistData: Resource
var SD: Resource
var SessData: Resource
var Video: Resource 
var Audio: Resource 
var Misc: Resource 

var TheContainer: Node
var Networking: Node 

var BackAndEscape: Label
var LoadingScreen: PanelContainer
var InitialLoadingOutput: Label
var InitialLoadingSpinner

var current_state: String = ""
var current_state_ref: Node
var previous_state: String = ""
var the_state_stack: Array = []

var screenshot_finished: bool = false
var visual_loading_stopped_for_screenshot: bool = false

var splash_screen_loaded: bool = false
var submodules_ready: bool = false
signal submodules_loaded
var helpers_loaded: bool = false
var back_request_counter: int = 0
var continue_spinner_updates: bool = true
var theme_loaded: bool = false
var command_line_args: Dictionary = {}
var parsed_command_line_args_once: bool = false

var background_threads: Dictionary = {}
var background_loaded_resources: Dictionary = {}
var thread_counter = 0
var queued_threads: Dictionary = {}

var one_time_draw_check = false
var postsplash_was_null = false

var registered_quit_close_handler = Callable(self, "default_quit_handler")

var statics = {PM_the_first = false, }  # Use Sparringly and watch out for name conflicts.

@onready var MSC: Node = $MSC # Multi Scene Code
@onready var MultiSceneCode: Node = $MSC

func _ready():
    if not PM.statics.PM_the_first:
        PM.statics.PM_the_first = true
    else:
        printerr("PM has already been loaded and statics holds a PM_the_first"
            + " set to true already."
        )
        PM.get_tree().quit(ERR_ALREADY_EXISTS)
    var test_if_postsplash_was_null = get_tree().root.get_node_or_null("PostSplashSpeed")
    if test_if_postsplash_was_null == null:
        postsplash_was_null = true
        

func _draw():
    if not one_time_draw_check:
        one_time_draw_check = true
        call_deferred("load_submodule_pd_n_sd")

func _process(_delta) -> void:
    pass

func _notification(what):
    if what == NOTIFICATION_PREDELETE:
        if Networking != null:
            if is_instance_valid(Networking):
                Networking.free()
    if what == NOTIFICATION_WM_CLOSE_REQUEST:
        registered_quit_close_handler.call()
    if what == NOTIFICATION_WM_GO_BACK_REQUEST:
        pass
    if what == NOTIFICATION_APPLICATION_RESUMED:
        pass
    if what == NOTIFICATION_APPLICATION_PAUSED:
        pass

func detect_vital_data():
    if OS.get_processor_count() - 1 < 1:
        max_threads = 1
    else:
        max_threads = OS.get_processor_count() - 1
    if fake_os == "":
        operating_system = OS.get_name()
    else:
        operating_system = fake_os
    if fake_unique_id == "":
        unique_id = OS.get_unique_id()
    else:
        unique_id = fake_unique_id
    display_metrics = Video.get_display_metrics()
    set_container_n_get_export_vars()
    
    
func Log(to_print, level="debug"):
    if DEBUG_LEVELS.find(level) >= debug_level:
        if DEBUG_LEVELS.find(level) < 3:
            print(to_print)
        else:
            printerr(to_print)
        if debug_output_to_gui:
            get_tree().call_group("DebugOutput", "put", to_print)
        elif level == "testing":
            get_tree().call_group("DebugOutput", "put", to_print)

func ensure_container_start_vital_data():
    if postsplash_was_null:
        playing_custom_scene = true
        if OS.get_environment("CI_TESTING") != "":
            ci_testing = true
    # Load the container here.
    BGLoad.load_n_callback(
        "res://TheContainer.tscn", self, 
        "add_container_to_scene_tree"
    )

func add_container_to_scene_tree(result):
    if result[0] == "finished":
        var container_instance = result[1].instantiate()
        await get_tree().process_frame
        get_tree().root.add_child(container_instance)
        splash_screen_loaded = true
        call_deferred("detect_vital_data")

func set_container_n_get_export_vars():
    TheContainer = get_tree().root.get_node_or_null("Container")
    #Backup/Copy the variables across
    fake_os = TheContainer.fake_os
    fake_unique_id = TheContainer.fake_unique_id
    erase_user_dir = TheContainer.erase_user_dir
    run_unit_tests = TheContainer.run_unit_tests
    run_specified_test = TheContainer.run_specified_test
    artificial_delay_between_states = TheContainer.artificial_delay_between_states
    debug_level = TheContainer.debug_level
    if VERSION != TheContainer.VERSION:
        version_mismatch = true
    VERSION = TheContainer.VERSION
    VERSION_INT = TheContainer.VERSION_INT
    update_boot_image = TheContainer.update_boot_image
    LoadingScreen = TheContainer.get_node("Loading/Loading")
    get_tree().root.get_node("PostSplashSpeed").queue_free()
    if OS.has_feature("standalone"):
        running_as_export = true
    else:
        if version_mismatch:
            update_boot_image = true
            if FileAccess.file_exists("res://EverGreen/ProgramManager.gd"):
                var file = FileAccess.open("res://EverGreen/ProgramManager.gd", 
                    FileAccess.READ_WRITE
                )
                var file_lines_array = []
                var version_found = false
                var version_int_found = false
                while not file.eof_reached(): 
                    var line = file.get_line()
                    if not version_found:
                        if "var VERSION = " in line:
                            line = 'var VERSION = "' + TheContainer.VERSION + '"'
                            version_found = true
                    if not version_int_found:
                        if "var VERSION_INT = " in line:
                            line = ("var VERSION_INT = " + str(TheContainer.VERSION_INT) +
                                "  # Increment any time the micro changes."
                            )
                            version_int_found = true
                    file_lines_array.append(line)
                var joined_strings = "\n".join(PackedStringArray(file_lines_array))
                file.seek(0)
                file.store_string(joined_strings)
                file.close()
        if update_boot_image:
            # Take new screenshot:
            # Must yield to allow _ready() for TheContainer to execute.
            Log("Performance Warning: Updating Boot Image Screenshot.", "warning")
            while not splash_screen_loaded:
                await get_tree().process_frame  
            LoadingScreen.Version.text = VERSION
            LoadingScreen.Spinner.modulate.a = 0
            LoadingScreen.GameHint.modulate.a = 0
            LoadingScreen.ItemOutput.modulate.a = 0
            LoadingScreen.BootSpeed.modulate.a = 0
            # Wait for the frame to re-draw for the changes made.
            await get_tree().process_frame
            # Wait until the frame has finished before getting the texture.
            await RenderingServer.frame_post_draw
            # Retrieve the captured image.
            var img = get_viewport().get_texture().get_image()
            img.save_png("res://assets/images/Sprites/boot_splash.png")
            LoadingScreen.Spinner.modulate.a = 1
            LoadingScreen.GameHint.modulate.a = 1
            LoadingScreen.ItemOutput.modulate.a = 1
            LoadingScreen.BootSpeed.modulate.a = 1
    call_deferred("check_load_entry_point")

func load_submodule_pd_n_sd():
    BGLoad.load_n_callback(
        "res://EverGreen/Settings.gd", self, 
        "load_submodule_video"
    )

func load_submodule_video(result):
    if result[0] == "finished":
        PD = result[1].new("LongTermData", "", true, true)
        PersistData = PD
        await get_tree().process_frame
        SD = result[1].new("ShortTermData")
        SessData = SD
        BGLoad.load_n_callback(
            "res://EverGreen/Video.gd", self, 
            "load_submodule_audio"
        )

func load_submodule_audio(result):
    if result[0] == "finished":
        Video = result[1].new()
        BGLoad.load_n_callback(
            "res://EverGreen/Audio.gd", self, 
            "load_submodule_misc"
        )

func load_submodule_misc(result):
    if result[0] == "finished":
        Audio = result[1].new()
        BGLoad.load_n_callback(
            "res://EverGreen/Misc.gd", self, 
            "load_submodule_networking"
        )

func load_submodule_networking(result):
    if result[0] == "finished":
        Misc = result[1].new()
#        BGLoad.load_n_callback(
#            "res://EverGreen/Networking/Networking.tscn", self, 
#            "finish_submodule_loading"
#        )
        call_deferred("finish_submodule_loading", null)

func finish_submodule_loading(_result):
#    if result[0] == "finished":
#        Networking = result[1].instantiate()
#        add_child(Networking)
    call_deferred("ensure_container_start_vital_data")

func check_load_entry_point():
    if not visual_loading_stopped_for_screenshot:
        LoadingScreen.item_name("ProgramManager States")
    Log("Program Manager: State Loading Completed.")
    submodules_ready = true
    emit_signal("submodules_loaded")
    if not playing_custom_scene:
        LoadingScreen.fade_in()
        LoadingScreen.progress(0.1)
        LoadingScreen.item_name(TheContainer.entry_point_scene)
        BGLoad.load_n_callback(TheContainer.entry_point_scene, self, 
            "load_entry_point"
        )
    else:
        LoadingScreen.fade_out()

func load_entry_point(results):
    if results[0] == "finished":
        var loaded_scene = results[1].instantiate()
        if artificial_delay_between_states != 0:
            await get_tree().create_timer(artificial_delay_between_states).timeout
        PM.TheContainer.assign_as_next_scene(loaded_scene)
        await get_tree().process_frame
        PM.LoadingScreen.progress(100.0)
        PM.LoadingScreen.fade_out()
    elif results[0] == "loading":
        LoadingScreen.progress(results[1] * 100.0)

################################################################################
# Application Life-cycle States for EverGreen tracked via current and previous
# previous == "Quitting" then Starting->Running
# previous == "ForegroundPaused"or"BackgroundPaused" then Resuming->ForegroundPaused

func default_quit_handler():
    get_tree().quit()


