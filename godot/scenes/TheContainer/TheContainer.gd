@tool
extends Node
@export_file("*.tscn") var entry_point_scene = ""
# Major.Minor.Micro-alpha, beta, rc, preview, dev
@export var VERSION: String = "0.1.0-dev0"
@export var VERSION_INT: int = 1  # Increment any time the micro changes.
@export var update_boot_image: bool = false
@export_group("Testing & Debugging")
@export var fake_os: String = "" # (String, "Android")
@export var fake_unique_id: String = "" # (String, "FAKED8UNIQUE8ID8NOT8REAL8AT8ALL")
@export var erase_user_dir: bool = false
@export var run_unit_tests: bool = false
@export var run_specified_test: bool = false
@export var artificial_delay_between_states: float = 0
@export_enum("not_set", "debug", "info", "warning", "error", "critical", 
    "testing") var debug_level: int = 3

@export_group("Networking")
@export var Networking_same_IP_host_OK: bool = false
@export var Networking_use_same_game_name: bool = true
@export var Networking_use_this_game_name: String = ""

# Declare member variables here.
var current_scene
var previous_scene
var active_scene_container_is_0: bool = false
var initialized = false
var ui_child_offset: Vector2

var SuperSystems: Dictionary = {}
var supersystem_execution_order: Array = []

func _ready():
    if Engine.is_editor_hint():
        # Code to execute in editor.
        on_editor_ready()
    else:
        # Code to execute in game.
        if not initialized:
            current_scene = $Scene0
            add_to_group("Container")
            initialized = true

func on_editor_ready():
    # Currently supporting only the following layouts:
    # 16:9 == 1920x1080
    # 9:16 == 540x960
    # 4:3 == 800x600
    if ProjectSettings.has_setting("display/window/size/viewport_width"):
        if ProjectSettings.get_setting("display/window/size/viewport_width") == 540:
            $"%TheCamera".offset.x = 270
        elif ProjectSettings.get_setting("display/window/size/viewport_width") == 1920:
            $"%TheCamera".offset.x = 960
        elif ProjectSettings.get_setting("display/window/size/viewport_width") == 800:
            $"%TheCamera".offset.x = 400
    if ProjectSettings.has_setting("display/window/size/viewport_height"):
        if ProjectSettings.get_setting("display/window/size/viewport_height") == 960:
            $"%TheCamera".offset.y = 480
        elif ProjectSettings.get_setting("display/window/size/viewport_height") == 1080:
            $"%TheCamera".offset.y = 540
        elif ProjectSettings.get_setting("display/window/size/viewport_height") == 600:
            $"%TheCamera".offset.y = 300

func next_screen(screen_jumps):
    var xy = screen_jumps.split_floats(",")
    get_tree().call_group("Camera3D", "instant_pan_camera", int(xy[0]), int(xy[1]))
    if PM.STD.get_("previous_screen_jumps") != PM.STD.get_("current_screen"):
        if PM.STD.get_("current_screen") != null:
            PM.STD.set_("previous_screen_jumps", PM.STD.get_("current_screen"))
    call_deferred("offset_UI_child", xy)
    PM.STD.set_("current_screen", screen_jumps)

func offset_UI_child(offsets):
    if $UI.get_child_count() != 0:
        var calc_offset_x = offsets[0] * 540 * 1
        var calc_offset_y = offsets[1] * 960 * 1
        $UI.get_child(0).position.x = calc_offset_x
        $UI.get_child(0).position.y = calc_offset_y
        ui_child_offset = $UI.get_child(0).position

func offset_UI_for_virtual_keyboard(keyboard_height):
    $UI.get_child(0).position = ui_child_offset
    $UI.get_child(0).position.y += keyboard_height

func assign_as_next_scene(scene_ref, retain_previous=false) -> void:
    if active_scene_container_is_0:
        active_scene_container_is_0 = false
        $Scene1.add_child(scene_ref)
        var other_child_count = $Scene0.get_child_count()
        call_deferred("finish_assignment_hide_loading")
        if other_child_count == 0:
            return
        # else:
        var other_child = $Scene0.get_child(0)
        other_child.visible = false
        if not retain_previous:
            other_child.queue_free()
        return
    # else:
    active_scene_container_is_0 = true
    $Scene0.add_child(scene_ref)
    var other_child_count = $Scene1.get_child_count()
    call_deferred("finish_assignment_hide_loading")
    if other_child_count == 0:
        return
    # else:
    var other_child = $Scene1.get_child(0)
    other_child.visible = false
    if not retain_previous:
        other_child.queue_free()
    return

func finish_assignment_hide_loading() -> void:
    PM.LoadingScreen.progress(100.0)
    PM.LoadingScreen.fade_out()
