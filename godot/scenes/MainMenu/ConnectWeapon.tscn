[gd_scene load_steps=8 format=3 uid="uid://ckyrl21c6sxim"]

[ext_resource type="PackedScene" uid="uid://dj3w5htwyakpl" path="res://scenes/Global/NoHeaderTemplate.tscn" id="1_g1d3l"]
[ext_resource type="Texture2D" uid="uid://dnfv5js451qr0" path="res://assets/images/Sprites/pencil_w.png" id="2_t4neg"]
[ext_resource type="Theme" uid="uid://1fujmt2b3c8n" path="res://assets/themes/default.tres" id="3_o5k62"]
[ext_resource type="Texture2D" uid="uid://4d1b8b7ftjwg" path="res://assets/images/Sprites/rk45_color.png" id="4_wy4mv"]
[ext_resource type="Texture2D" uid="uid://c6sfdmdcpbe1g" path="res://assets/images/Sprites/sr12_color.png" id="5_lp6ap"]

[sub_resource type="GDScript" id="GDScript_3gcjq"]
resource_name = "ConnectWeapon"
script/source = "extends VBoxContainer

var prune_older_than: float = 10.0

signal nearby_weapons_updated
var weapons_nearby_list: Dictionary = {}

@onready var OverlayVBox = $\"../OverlayVBox\"
@onready var OverlayStatusLabel = $\"../OverlayVBox/ColorRect/VBoxContainer/OverlayStatusLabel\"
@onready var OverlayPistolImage = $\"../OverlayVBox/ColorRect/VBoxContainer/PistolImage\"
@onready var OverlayRiffleImage = $\"../OverlayVBox/ColorRect/VBoxContainer/RiffleImage\"

@onready var WeaponContainer = $\"VBoxContainer2/Panel/ScrollContainer/VBoxContainer\"
@onready var ConnectWpnBtn = $VBoxContainer2/ConnectWeaponBtn
@onready var TestFireWpnBtn = $VBoxContainer2/HBoxContainer3/TestFireBtn
@onready var ContinueBtn = $VBoxContainer2/HBoxContainer3/ContinueBtn
@onready var ConnectionStatus = $VBoxContainer2/ConnectionStatus
@onready var ConnectionNotes = $VBoxContainer2/ConnectionNotes

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    while PM.submodules_ready == false:
        await get_tree().process_frame
    #Status of connection to weapon
    PM.SD.connect(PM.SD.mon_(\\
            \"fi_laser_is_connected\"), Callable(self, \"connection_status_update\"))
    #List of weapons found on bluetooth scan
    PM.SD.connect(PM.SD.mon_(\\
            \"fi_weapons_found\"), Callable(self, \"update_weapons_found_list\"))
    connect(\"nearby_weapons_updated\", Callable(self, \"update_weapons_displayed\"))
    OverlayVBox.show()
    if PM.SD.get_(\"fi_laser_id\") == null:
        PM.SD.set_(\"fi_laser_id\", 0)
    PM.MSC.get_node(\"FreecoiLInterface\").start_bt_scan()
    call_deferred(\"update_message_after_delay\")
    call_deferred(\"prune_old_weapons\")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
    pass

func update_message_after_delay():
    await get_tree().create_timer(0.2).timeout
    OverlayStatusLabel.text = \"Scanning for neaby Bluetooth devices\\nPlease Wait...\"
    
func update_weapons_found_list(updated_list):
    if updated_list == null:
        return
    if OverlayVBox.visible:
        OverlayVBox.hide()
    var updated_nearby_list = false
    for key in updated_list:
        var value = updated_list[key]
        if not weapons_nearby_list.has(key):
            updated_nearby_list = true
            weapons_nearby_list[key] = value
        elif weapons_nearby_list[key][1] != value[1]:
            updated_nearby_list = true
            weapons_nearby_list[key] = value
    if updated_nearby_list:
        emit_signal(\"nearby_weapons_updated\")

func update_weapons_displayed() -> void:
    for key in weapons_nearby_list:
        var wpn_btn = WeaponContainer.get_node_or_null(key)
        var value = weapons_nearby_list[key]
        if wpn_btn == null:
            var new_btn = Button.new()
            new_btn.name = key
            new_btn.set_meta(\"mac_address\", value[0])
            new_btn.set_meta(\"last_time_spotted\", value[1])
            new_btn.text = key
            new_btn.toggle_mode = true
            new_btn.connect(\"pressed\", Callable(self, \"weapon_button_selected\").bind(new_btn))
            WeaponContainer.add_child(new_btn)
        else:
            wpn_btn.set_meta(\"last_time_spotted\", value[1])

func weapon_button_selected(btn) -> void:
    for child in WeaponContainer.get_children():
        if child != btn:
            child.button_pressed = false
    btn.button_pressed = true
    PM.SD.set_(\"fi_weapon_highlighted\", btn.name)
    ConnectWpnBtn.disabled = false

func prune_old_weapons() -> void:
    while true:
        await get_tree().create_timer(1.0).timeout
        for child in WeaponContainer.get_children():
            if Time.get_unix_time_from_system() - child.get_meta(\"last_time_spotted\") > prune_older_than:
                if child.button_pressed:
                    # If the button was selected/highlighted and is being pruned
                    # then we need to disable the connect option as the weapon
                    # has disappeared.
                    ConnectWpnBtn.disabled = true
                child.queue_free()

func _on_connect_weapon_btn_pressed() -> void:
    ConnectWpnBtn.disabled = true
    PM.MSC.get_node(\"FreecoiLInterface\").connect_to_highlighted_laser_gun(
        PM.SD.get_(\"fi_weapon_highlighted\")
    )

func _on_test_fire_btn_pressed() -> void:
    if PM.SD.get_(\"fi_laser_is_connected\") == 2:
        PM.MSC.get_node(\"FreecoiLInterface\").fire_weapon(PM.SD.get_(\"fi_laser_id\"))

func connection_status_update(update) -> void:
    print(\"Connection Status Update: \", update)
    match update:
        1: 
            ConnectionStatus.text = \"Connecting\"
            ConnectionNotes.text = \"Attempting\\nConnection\"
            TestFireWpnBtn.disabled = true
            ContinueBtn.text = \"Continue Without\\nWeapon\"
        2: 
            ConnectionStatus.text = \"Connected\"
            ConnectionNotes.text = \"Connection\\nEstablished\"
            TestFireWpnBtn.disabled = false
            ContinueBtn.text = \"Continue With\\nWeapon\"
        3: 
            ConnectionStatus.text = \"Disconnected\"
            ConnectionNotes.text = \"Connection to\\ndevice lost\"
            TestFireWpnBtn.disabled = true
            ContinueBtn.text = \"Continue Without\\nWeapon\"

func _on_continue_btn_pressed() -> void:
    TestFireWpnBtn.disabled = true
    ContinueBtn.disabled = true
    ConnectWpnBtn.disabled = true
    get_parent().goto_scene(\"res://scenes/Global/NoHeaderTemplate.tscn\")
"

[sub_resource type="StyleBoxFlat" id="StyleBoxFlat_pf1dx"]
bg_color = Color(0, 0, 0, 1)
border_width_left = 4
border_width_top = 4
border_width_right = 4
border_width_bottom = 4
border_color = Color(0.733333, 0.266667, 0.188235, 1)

[node name="MarginContainer" instance=ExtResource("1_g1d3l")]

[node name="VBoxContainer" parent="." index="0"]
script = SubResource("GDScript_3gcjq")

[node name="TitleLabel" parent="VBoxContainer/VBoxContainer2/VBoxContainer" index="0"]
text = "Connect A Weapon"

[node name="TextLabel" parent="VBoxContainer/VBoxContainer2/HBoxContainer" index="0"]
text = "↓ Available Weapons ↓"

[node name="Panel" type="Panel" parent="VBoxContainer/VBoxContainer2" index="2"]
custom_minimum_size = Vector2(0, 250)
layout_mode = 2
theme_override_styles/panel = SubResource("StyleBoxFlat_pf1dx")

[node name="ScrollContainer" type="ScrollContainer" parent="VBoxContainer/VBoxContainer2/Panel" index="0"]
custom_minimum_size = Vector2(0, 240)
layout_mode = 2
offset_top = 8.0
offset_right = 540.0
offset_bottom = 258.0

[node name="VBoxContainer" type="VBoxContainer" parent="VBoxContainer/VBoxContainer2/Panel/ScrollContainer" index="0"]
layout_mode = 2
size_flags_horizontal = 3

[node name="HBoxContainer2" type="HBoxContainer" parent="VBoxContainer/VBoxContainer2" index="3"]
layout_mode = 2
theme_override_constants/separation = 6
alignment = 1

[node name="CustomWeaponName" type="LineEdit" parent="VBoxContainer/VBoxContainer2/HBoxContainer2" index="0"]
custom_minimum_size = Vector2(400, 0)
layout_mode = 2
placeholder_text = "Custom Weapon Name"
alignment = 1
editable = false
clear_button_enabled = true

[node name="TextureRect" type="TextureRect" parent="VBoxContainer/VBoxContainer2/HBoxContainer2" index="1"]
layout_mode = 2
texture = ExtResource("2_t4neg")
expand_mode = 3

[node name="ConnectionStatus" type="Label" parent="VBoxContainer/VBoxContainer2" index="4"]
layout_mode = 2
text = "Connection Status"
horizontal_alignment = 1

[node name="ConnectionNotes" type="Label" parent="VBoxContainer/VBoxContainer2" index="5"]
layout_mode = 2
text = "Connection
Notes"
horizontal_alignment = 1

[node name="ConnectWeaponBtn" type="Button" parent="VBoxContainer/VBoxContainer2" index="6"]
layout_mode = 2
disabled = true
text = "Connect Selected Weapon"

[node name="HBoxContainer3" type="HBoxContainer" parent="VBoxContainer/VBoxContainer2" index="7"]
layout_mode = 2
theme_override_constants/separation = 80
alignment = 1

[node name="TestFireBtn" type="Button" parent="VBoxContainer/VBoxContainer2/HBoxContainer3" index="0"]
layout_mode = 2
disabled = true
text = "Test
 Fire "

[node name="ContinueBtn" type="Button" parent="VBoxContainer/VBoxContainer2/HBoxContainer3" index="1"]
layout_mode = 2
disabled = true
text = "Continue Without
Weapon"

[node name="OverlayVBox" type="VBoxContainer" parent="." index="1"]
visible = false
layout_mode = 2
theme = ExtResource("3_o5k62")
alignment = 1

[node name="ColorRect" type="ColorRect" parent="OverlayVBox" index="0"]
layout_mode = 2
size_flags_vertical = 3
color = Color(0, 0, 0, 0.901961)

[node name="VBoxContainer" type="VBoxContainer" parent="OverlayVBox/ColorRect" index="0"]
layout_mode = 1
anchors_preset = 15
anchor_right = 1.0
anchor_bottom = 1.0
grow_horizontal = 2
grow_vertical = 2
alignment = 1

[node name="OverlayStatusLabel" type="Label" parent="OverlayVBox/ColorRect/VBoxContainer" index="0"]
layout_mode = 2
text = "Starting Bluetooth Scan
Please Wait..."
horizontal_alignment = 1
autowrap_mode = 2

[node name="PistolImage" type="TextureRect" parent="OverlayVBox/ColorRect/VBoxContainer" index="1"]
visible = false
layout_mode = 2
texture = ExtResource("4_wy4mv")
stretch_mode = 3

[node name="RiffleImage" type="TextureRect" parent="OverlayVBox/ColorRect/VBoxContainer" index="2"]
visible = false
layout_mode = 2
texture = ExtResource("5_lp6ap")
stretch_mode = 3

[connection signal="pressed" from="VBoxContainer/VBoxContainer2/ConnectWeaponBtn" to="VBoxContainer" method="_on_connect_weapon_btn_pressed" flags=3]
[connection signal="pressed" from="VBoxContainer/VBoxContainer2/HBoxContainer3/TestFireBtn" to="VBoxContainer" method="_on_test_fire_btn_pressed" flags=3]
[connection signal="pressed" from="VBoxContainer/VBoxContainer2/HBoxContainer3/ContinueBtn" to="VBoxContainer" method="_on_continue_btn_pressed" flags=3]

[editable path="VBoxContainer/VBoxContainer2/VBoxContainer"]
